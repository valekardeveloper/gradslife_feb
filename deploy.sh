echo "Take the backup of uploads...."
cd ~/project/gradslife
mkdir -p ./data/uploads
mkdir -p ~/backup
#rm -r ~/backup/uploads/
mv ./data/uploads ~/backup/uploads
cd ~
echo "Backing up complete.."


echo "Recreating project folder...."
rm -rf project
mkdir project
cd ~/project

################################# get the projects #########################################
echo "Cloning gradslife project ...."
rm -rf gradslife
git clone git@bitbucket.org:valekardeveloper/gradslife.git
##########################################################################################
cd gradslife

################################Check if nginx-proxy already exists#########################
echo "Checking if nginx-proxy is existing, if not create a new nginx-proxy"

nginx_proxy="nginx-proxy"
ngnx=$(docker ps -a | awk -v word=$nginx_proxy '$0~word')
nginx_container_ID=$(echo $ngnx | awk '{print $1}')

if [[ -z "$nginx_container_ID" ]]; then 
    echo "Nginx proxy does not exist"
    cd nginx-proxy
    echo "creating network nginx-proxy.."
    docker network create "nginx-proxy"
    echo "copying the certificates..."
    cp -r ~/certs/ ~/project/gradslife/nginx-proxy/
    cp -r ~/certs/ ~/project/gradslife/wordpress/
    docker-compose up -d
    cd ..
fi
######################################################################################




##############################Remove existing wordpress container#####################
echo "Removing existing wordpress containers...."
wordpress="wordpress"
x=$(docker ps -a | awk -v word=$wordpress '$0~word' )
container_ID=$(echo $x | awk '{print $1}')
image_name=$(echo $x | awk '{print $2}')
echo $container_ID 
echo $image_name
#remove docker container

    mkdir -p ~/project/gradslife/wordpress/data/uploads
if [[ ! -z "$container_ID" ]]; then   
    echo "Now copying uploads file from container to data uploads"
    docker cp $container_ID:/var/www/html/wp-content/uploads/ ./data/uploads/
	docker stop $container_ID
	docker rm $container_ID
	#remove docker image
	docker rmi $image_name
fi
#########################################################################################



###################Creating the new wordpress containers#################################
cd wordpress
docker-compose up --force-recreate -d

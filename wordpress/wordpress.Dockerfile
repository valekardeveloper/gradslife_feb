FROM wordpress:latest 
COPY ./wp-content/themes /var/www/html/wp-content/themes
RUN mkdir -p /var/www/html/wp-content/uploads && chmod 777 /var/www/html/wp-content/uploads
COPY ./data/uploads/. /var/www/html/wp-content/uploads/